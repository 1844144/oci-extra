function basename(path) {
	if (path)return path.split('/').reverse()[0];
	else return path;
}

function fix_top_menu_behavior(activation_selector, selector_list ) {
			address = jQuery(activation_selector).attr('href');
			address = basename(address);
			if (address && jQuery('a.tab-link[href='+address+']'  ).length > 0){

	   			jQuery(selector_list).click(function(ev){
	   					ev.preventDefault();
		   				el = jQuery(this);
		   				var href = el.attr('href');
		   				if (href) {
		   					addr = basename(href);
			   				jQuery('a.tab-link[href='+addr+']'  ).click();
	   					}
	   			});


			}

}

function click_on_top_menu(links, where_to_find ){
	jQuery(links).click( function(){
		href = jQuery(this).attr('href');
		jQuery(where_to_find).find("a[href='"+href+"']").click();

	});
}


jQuery(function(){
			// check if button exists
			fix_top_menu_behavior('#menu-item-9269>a','#menu-item-9269>a,#menu-item-9270>a,#menu-item-9271>a');
			fix_top_menu_behavior('#menu-item-12162>a','#menu-item-12162>a,#menu-item-12163>a,#menu-item-12164>a,#menu-item-12165>a');
			click_on_top_menu('.fusion-footer a[href*=9c752952d18e721aed4],.fusion-footer a[href*=9ba80a5589a53279828],.fusion-footer a[href*=16248bbc8e70199260f]','#menu-item-8301');
			

});